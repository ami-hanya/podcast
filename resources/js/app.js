require('./bootstrap');
import Vue from 'vue';
import router from './router';
import store from './store/index';
import Welcome from './components/Welcome.vue';
import VueRouter from 'vue-router';
import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all.js'
import { types as authType } from "./store/modules/Auth.module";
import Header from './components/pages/Header.vue';
import wysiwyg from "vue-wysiwyg";

Vue.use(VueRouter);
store.dispatch(authType.GET);
Vue.component('welcome', Welcome);
Vue.component('myheader',Header);
Vue.use(store);
Vue.use(wysiwyg, {forcePlainTextOnPaste: true});

const app = new Vue({
    el: '#app',
    router,
    store
});
export default app;