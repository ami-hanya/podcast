import i18n from "dev/lang";

function $t(key) {
	return i18n.t(key);
}

export const types = {
	NO_SCHEDULING: 0,
	SCHEDULING: 1,
	COORDINATED: 2,
	DONE:4,
	NOT_DONE:7,
	DO_AGAIN:8,
	CANCEL:5,
	NOT_READY:10
};

const status = [
	{id: types.NO_SCHEDULING, text: $t("statusManager.notPlaid"), color: "#90CAF9"},
	{id: types.SCHEDULING, text: $t("statusManager.notPlaid"), color: "#FFCC80"},
	{id: types.COORDINATED, text: $t("statusManager.coordinated"), color: "#FFF59D"},
	{id: types.DONE, text: $t("statusManager.done"), color: "#B0C4DE"},
	{id: 9, text: $t("statusManager.waiting"), color: "#9FA8DA"},
	{id: types.CANCEL, text:$t("statusManager.canceled"), color: "#EF9A9A"},
	{id: types.NOT_DONE, text: $t("attributes.notSurveyed"), color: "#CE93D8"},
	{id: types.DO_AGAIN, text: $t("statusManager.other"), color: "#BDB76B"},
	{id: types.NOT_READY, text: $t("statusManager.noReady"), color: "#CC66FF"}

];

export default {

	get status() {
		return status;
	},
	text(id) {
		let status = this.getStatusById(id);
		return status ? status.text : "";
	},

	getClientList(){
		// hide status text for clients user
		let temp = status.slice(0);
		temp.find(item => item.id ===types.NO_SCHEDULING).text ="בטיפול";
		temp.find(item => item.id ===types.SCHEDULING).text ="בטיפול";
		return status;
	},

	getStatusById(id) {
		return this.status.find(function (status) {
			return (status.id === id);
		});
	},
	color(id) {
		let status = this.getStatusById(id);
		return status ? status.color : null;
	},


};
