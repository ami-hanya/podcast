import Vue from "vue";
import Vuex from "vuex";
import PodcastModule from './modules/Podcast.module';
import AuthModule from './modules/Auth.module';
import UserModule from './modules/User.module';
import AboutModule from './modules/About.module';

Vue.use(Vuex);
const store = new Vuex.Store({
  state: {
    loading: false
  },
  modules: {
    PodcastModule,
    AuthModule,
    UserModule,
    AboutModule
  }
});
export default store;

export const config = function (key, value) {
  store.commit(ConfigModule.types.SET, { key, value });
};
