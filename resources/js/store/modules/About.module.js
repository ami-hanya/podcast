import AboutService from "../../services/AboutService";
import ModuleCreator from "../ModuleCreator";

export const types = {
};
export const params = {
	title:"",
	body:"",
	image:""

};

/**
@namespace
@property  {object} module,
@property  {object} module.state,
@property  {object} modules.client,
@property  {array}  modules.clients
**/

const module = {
	state: {
		
	},
	mutations: {
	},
	getters: {},
	actions: {},
	types: types
};
export default ModuleCreator("about", AboutService, module, types ,params);
