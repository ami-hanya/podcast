import store from "dev/store";
import {Config} from "dev/store/modules/Config.module";

export const types = {
	SURVEYORS:4 ,
	OFFICERS:3,
	BRANCH_MANAGER: 7
};

function getRoles() {
	return store.getters.rolesConfig.roles;
}

function findTheRole(roleId) {
	return getRoles().find(role => role.id === roleId);
}

function getTheRule(roleId) {
	let role = findTheRole(roleId);
	return store.getters.rolesConfig.rules[role.rule];
}



function updateConfig(user) {
	let role = getTheRule(user.role);
	if (role.route) {
		Config("routes.default", role.route);
	}
	if (role.baseUrl) {
		Config("request.baseUrl", role.baseUrl);
	}
}

store.watch((state) => state.AuthModule.user, (user) => {
	updateConfig(user);
});

export default {
	get roles() {
		return getRoles();
	},
	toString(id){
		return findTheRole(id).text;
	},
	company(){
		return getRoles().filter(role => role.id <= 4);
	},
	clients() {
		return getRoles().filter(item => item.id >= 5);
	},
	branch() {
		return getRoles().filter(item => item.id >= 7);
	},
	getRule(ruleText) {
		return this.getRuleById(store.state.AuthModule.user, ruleText);
	},
	getRuleById(role, ruleText) {
		let rule = getTheRule(role);
		return ruleText ? rule[ruleText] : rule;
	},
	getRoleById:findTheRole,
	needBranch(id) {
		return id >= 7;
	},
	needClient(id) {
		return id >= 5;
	},

};
