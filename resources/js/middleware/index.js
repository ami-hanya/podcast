import RouterMiddleware from "./RouterMiddleware";
import AuthMiddleware from "./AuthMiddleware";
import AlertMiddleware from "./AlertMiddleware";
import GuestMiddleware from "./GuestMiddleware";

export default new RouterMiddleware({
  "RequireAuth": AuthMiddleware,
  "auth": AuthMiddleware,
  "guest": GuestMiddleware,
  "alert": AlertMiddleware
});
