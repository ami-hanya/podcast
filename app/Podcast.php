<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Podcast extends Model
{
    protected $fillable = [
        'title', 'description','image','soundcloud_link',
    ];
    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }
}
