<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(){
        $users = User::all();
        return $users;
    }
    public function show(Request $request,$id)
	{
		$user = User::findOrFail($id);
		$this->authorize("touch",$user);
		return response()->json($user);
    }

    public function store(Request $request)
    {

        $user = User::create($this->$request);
        return response()->json($user);
    }



    public function update(Request $request, $id)
	{

		$user = User::findOrFail($id);
		$this->authorize("touch",$user);

		$data = $request->all();
		if ($request->has("password")) {
			$data["password"] = bcrypt($request->password);
		}
		$user->update($data);
		return response()->json($user, 200);
	}

    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $this->authorize("touch",$user);
        $user->delete();
        return response()->json($user, 200);
    }
}
