import UserService from "../../services/UserService";
import ModuleCreator from "../ModuleCreator";

export const types = {
  UPLOAD_IMAGE: "user/edit"
};
export const params = {
  email: "",
  password: ""
};

/**
 @namespace
 @property  {object} module,
 @property  {object} module.state,
 @property  {object} modules.user,
 @property  {array}  modules.users
 **/

const module = {
  state: {},
  mutations: {},
  getters: {},
  actions: {},
  types: types
};
export default ModuleCreator("user", UserService, module, types, params);
