import VueRouter from "vue-router";
import Home from './components/pages/Home.vue';
import Login from './components/pages/Login.vue';
import PodcastCreate from './components/pages/PodcastCreate.vue';
import middleware from './middleware';
import Register from './components/pages/Register.vue';
import About from './components/pages/About.vue';
import EditAbout from './components/pages/EditAbout.vue';
import SinglePodcast from './components/pages/SinglePodcast.vue';

const routes = [
    {path:'/',component: Home,name:"home"} ,
    {path:'/about', component:About,name:'about'},
    {path:'/podcast/:id',component:SinglePodcast,name:'podcast'},
    {path:'/edit-about', component:EditAbout,name:'edit-about',meta: { "RequireAuth": true }},
    {path: '/admin', component:Login,name:"login"},
    {path:'/create-podcast', component:PodcastCreate, name:'create',meta: { "RequireAuth": true }},
    {path: '/register',component:Register,name:'register',  meta: { "RequireAuth": true } }
      
];

const router = new VueRouter({
    history: false,
    mode: "history",
    routes
  });

router.beforeEach((to, from, next) => {
    middleware.check(to, from, next);
});
  
  export default router;
  