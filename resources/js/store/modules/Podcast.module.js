import PodcastService from "../../services/PodcastService";
import ModuleCreator from "../ModuleCreator";

export const types = {
};
export const params = {
	
};

/**
@namespace
@property  {object} module,
@property  {object} module.state,
@property  {object} modules.client,
@property  {array}  modules.clients
**/

const module = {
	state: {
		
	},
	mutations: {
	},
	getters: {},
	actions: {
	},
	types: types
};
export default ModuleCreator("podcast", PodcastService, module, types ,params);
