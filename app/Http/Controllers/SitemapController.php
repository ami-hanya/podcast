<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Podcast;


class SitemapController extends Controller
{
    public function index() {
        $podcast = Podcast::all()->first();
        return response()->view('sitemap.index', [
            'podcast' => $podcast
        ])->header('Content-Type', 'text/xml');
    }

    public function podcasts() {
        $podcast = Podcast::latest()->get();
        return response()->view('sitemap.podcast', [
            'podcast' => $podcast,
        ])->header('Content-Type', 'text/xml');
    }
}
