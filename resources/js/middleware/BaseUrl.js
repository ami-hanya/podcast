import { setBaseUrl } from "../services/myAxios";

export default async (next, to, from) => {
  let baseUrl = to.meta.baseUrl;
  if (!baseUrl) {
    let find = to.matched.find(m => m.meta.baseUrl);
    if (find) {
      baseUrl = find.meta.baseUrl;
    }
  }
  setBaseUrl(baseUrl);
  return next();
};
