<?php


Route::group(['prefix' => 'api'], function () {
   Route::auth();
   Route::get("auth", "Auth\LoginController@auth");
   Route::post("auth", "Auth\RegisterController@register");
   Route::get("logout", "Auth\LoginController@logout");
   Route::get("login/user/{id}", "Auth\LoginController@insertAs");
    Auth::routes();
});
Route::get('/sitemap.xml', 'SitemapController@index')->name('sitemap.xml');
Route::get('/sitemap.xml/podcast', 'SitemapController@podcasts');
Route::Resource("api/about",'AboutController');
Route::Resource('api/podcast', 'PodcastController');
Route::get('/{any}', 'SinglePageController@index')->where('any', '.*');