import BaseService from "./BaseService";

export default class PodcastService extends BaseService {
	static get className() {
		return "podcast";
	}
};
