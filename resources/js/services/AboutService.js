import BaseService from "./BaseService";

export default class AboutService extends BaseService {
	static get className() {
		return "about";
	}
};
