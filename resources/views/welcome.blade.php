<!DOCTYPE html>
    <html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{csrf_token()}}">
        <meta name="google-site-verification" content="uwO9QrRBOsKe3jyMXCYNsv763uAEzy3YWicy0CapT4Y" />
        @if($podcast ?? "")
        <meta property="og:title" content="{{$podcast['title'] ?? ''}}" />
        <meta property="og:url" content="https://podcast.clap.co.il/?podcast={{$podcast['id'] ?? ''}}"/>
        <meta property="og:image"  content="https://podcast.clap.co.il/storage/images/{{ $podcast['image'] ?? ''}}" />
        <meta property="og:site_name" content=' כיפת תרבות - כיפתרבות פודקאסט תרבות יהודית' />
        <meta property="og:description" content="{{$podcast['description'] ?? ''}}"/>
        @endif
        @if($about ?? "")
        <meta property="og:title" content="{{$about['title'] ?? ''}}" />
        <meta property="og:url" content="https://podcast.clap.co.il/about }}"/>
        <meta property="og:image"  content="https://podcast.clap.co.il/storage/images/{{ $about['image'] ?? ''}}" />
        <meta property="og:site_name" content=' כיפת תרבות - כיפתרבות פודקאסט תרבות יהודית' />
        <meta property="og:description" content="{{$about['body'] ?? ''}}"/>
        @endif
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-119150593-9"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
                function gtag(){dataLayer.push(arguments);}
                gtag('js', new Date());
            gtag('config', 'UA-119150593-9');
        </script>
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" >
        <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
        <title> כיפת תרבות - כיפתרבות פודקאסט תרבות יהודית</title>
        <link href=" {{ mix('css/app.css') }}" rel="stylesheet">
    </head>
    <body>
        <div id="app">
            <welcome></welcome>

        </div>
        <script src="{{ mix('js/bootstrap.js') }}"></script>
        <script src="{{ mix('js/app.js') }}"></script>
    </body>
    </html>
