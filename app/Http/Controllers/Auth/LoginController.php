<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\User;

class LoginController extends Controller

{
   

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       $this->middleware('guest')->except('logout', "auth", "insertAs");
    }

    protected function authenticated(Request $request, $user)
	{
    
		return $user;
	}

    
	// protected function attemptLogin(Request $request)
	// {
    //     dd($request);
    //     $credentials = $this->credentials($request);
	// 	return $this->guard()->attempt($credentials,$request->filled('remember'));
    // }
    
    public function insertAs($userId)
	{
		$user = User::findOrFail($userId);
		$this->guard()->login($user);
		return $user;
	}

	/**
	 * @return Illuminate\Auth\GenericUser|null
	 */
	public function auth()
	{
		return Auth::user();
    }
    
    

}
