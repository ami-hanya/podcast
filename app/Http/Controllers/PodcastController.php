<?php

namespace App\Http\Controllers;
use App\Http\Requests\PodcastRequest;
use App\Podcast;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PodcastController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $podcasts = Podcast::orderBy("created_at","DESC")->paginate(10);
        return $podcasts;
    }

    public function getData(PodcastRequest $request){
        $data = $request->validated();
        $data['user_id'] = Auth::id();
        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(PodcastRequest $request)
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PodcastRequest $request, Podcast $podcast)
    {
        if($request->hasFile('image')){
        $uploadedFile = $request->file('image');
        $filename = pathinfo($uploadedFile ,PATHINFO_FILENAME);
        $ext = $request->file('image')->getClientOriginalExtension();
        $filenameToStore = $filename .'_'.time().'.'.$ext;
        $path = $uploadedFile->storeAs('images',$filenameToStore);
        }else{
            $filenameToStore =  "no Image";
        }
        $podcast = new Podcast;
        $podcast->title = $request->input('title');
        $podcast->description = $request->input('description');
        $podcast->soundcloud_link = $request->input('soundcloud_link');
        $podcast->user_id = auth()->user()->id;
        $podcast->image = $filenameToStore;
        $podcast->save();
        return response()->json($podcast);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Podcast  $podcast
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,Podcast $podcast)
    {
       
        $podcast = Podcast::find($podcast->id);
        return response()->json($podcast);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Podcast  $podcast
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,Podcast $podcast)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Podcast  $podcast
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $podcast = Podcast::find($id);
        if($request->hasFile('image')){
            $uploadedFile = $request->file('image');
            $filename = pathinfo($uploadedFile ,PATHINFO_FILENAME);
            $ext = $request->file('image')->getClientOriginalExtension();
            $filenameToStore = $filename .'_'.time().'.'.$ext;
            $path = $uploadedFile->storeAs('images',$filenameToStore);
        }else{
            $filenameToStore = $podcast->image;
        }
        $podcast->title = $request->input('title');
        $podcast->description = $request->input('description');
        $podcast->image = $filenameToStore;
        $podcast->soundcloud_link = $request->input('soundcloud_link');
        $podcast->id = $request->id;
        $podcast->user_id = auth()->user()->id;
        $podcast->save();
        return response()->json($podcast);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Podcast  $podcast
     * @return \Illuminate\Http\Response
     */
    public function destroy(Podcast $podcast)
    {
        $podcast->delete();
        return response()->json($podcast,200);

    }
}
