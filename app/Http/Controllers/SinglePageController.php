<?php

namespace App\Http\Controllers;

use App\About;
use Illuminate\Http\Request;
use App\Podcast;

class SinglePageController extends Controller
{
    public function index(Request $request, Podcast $podcast)
    {
        $about = '';
        $podcast = '';
        if ($request->podcast) {
            $podcast = Podcast::find($request->podcast);
            if (!$podcast) {
                $podcast = Podcast::all()->last();
                return redirect("/?podcast={$podcast->id}");
            }
            return view('welcome',compact('podcast'));
        }
        if (strcmp($request->requestUri,'/about')) {
            $about = About::all()->last();
            return view('welcome',compact('about'));
        }
        return view('welcome');
    }
}
