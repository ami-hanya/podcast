<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\About;

class AboutController extends Controller
{
    public function index()
    {
        $about = About::all()->last();
        return $about;
    }

    public function store(Request $request, About $about)
    {
        if($request->hasFile('image')){
        $uploadedFile = $request->file('image');
        $filename = pathinfo($uploadedFile ,PATHINFO_FILENAME);
        $ext = $request->file('image')->getClientOriginalExtension();
        $filenameToStore = $filename .'_'.time().'.'.$ext;
        $path = $uploadedFile->storeAs('images',$filenameToStore);
        }else{
            $filenameToStore =  "no Image";
        }
        $about = new About;
        $about->title = $request->input('title');
        $about->body = $request->input('body');
        $about->image = $filenameToStore;
        $about->save();
        return response()->json($about);
    }

    public function update(Request $request, About $about)
    {

        $about = About::findOrFail($request->id);
        if($request->hasFile('image')){
            $uploadedFile = $request->file('image');
            $filename = pathinfo($uploadedFile ,PATHINFO_FILENAME);
            $ext = $request->file('image')->getClientOriginalExtension();
            $filenameToStore = $filename .'_'.time().'.'.$ext;
            $path = $uploadedFile->storeAs('images',$filenameToStore);
        }else{
            $filenameToStore =  "no Image";
        }
        $about->update([
            "title" => $request->input('title'),
            "body" => $request->input('body'),
            "image" => $filenameToStore
        ]);
		return response()->json($about, 200);
    }

    public function show(Request $request,About $about)
    {
       
        $about = About::find($about->id);
        return response()->json($about);
    }

}
